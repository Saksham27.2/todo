// let addbutton = document.querySelector("#addbtn");
// const status = document.querySelector(".status");
// let inputbox = document.querySelector("#inputbox");
// let ulelement = document.querySelector(".elements");
// let editbutton=document.querySelector('.edit-icon');
// const allButton = status.querySelector("button:nth-of-type(1)");
// const completedButton = status.querySelector("button:nth-of-type(2)");
// const activeButton = status.querySelector("button:nth-of-type(3)");
// const clearCompletedButton = status.querySelector("button:nth-of-type(4)");

// let taskarray=[];

// function logAllTasks() {
//   taskarray.forEach(function(task, index) {
//     console.log(`Task ${index + 1}: ${task}`);
//   });
// }
// function add(e) {
//   e.preventDefault();
//   if (inputbox.value === "") {
//     alert("Write your task atleast");
//   } else {
//     let span = document.createElement("span");

//     span.innerHTML = '<i class="fas fa-trash"></i>';

//     let button = document.createElement("button");
//     button.textContent = "Edit";
//     button.className = "edit-icon";

//     let value = inputbox.value;

//     let para = document.createElement("p");
//     para.textContent = value;
//     para.className='li-para'

//     let li = document.createElement("li");
//     li.className='task-item';
//     li.appendChild(para);
//     li.appendChild(button);
//     li.appendChild(span);

//     button.addEventListener('click', () => editTask(li));
//     ulelement.appendChild(li);
//     inputbox.value = "";

//     taskarray.push(value);

//     updateTasks();
//     status.style.display = "flex";
//     logAllTasks();
//   }
// }

// function updateTasks() {
//   const remainingtasks = ulelement.querySelectorAll("li:not(.checked)").length;
//   status.querySelector(
//     "span p"
//   ).textContent = `Tasks Remaining : ${remainingtasks}`;
// }
// function editTask(li) {
//   let para = li.querySelector('.li-para');
//   let editbtn = li.querySelector('.edit-icon');

//   editbtn.addEventListener("click", function () {
//       para.setAttribute("contenteditable", "true");
//       para.focus();
//   });

//   para.addEventListener("keypress", function (event) {
//       if (event.key === "Enter") {
//           para.setAttribute("contenteditable", "false");

//           let index=Array.from(ulelement.children).indexOf(li);
//           taskarray[index]=para.textContent;
//       }
//   });
// }

// function intialstatus() {
//   status.style.display = ulelement.children.length === 0 ? "none" : "flex";
// }

// function InitialiseTasks()
// {
//   taskarray.forEach(task=>{
//     let span=document.createElement('span');
//     span.innerHTML='<i class="fas fa-trash"></i>';

//     let button=document.createElement('button');
//     button.textContent="Edit";
//     button.className='edit-icon';

//     let para =document.createElement('p');
//     para.textContent=task;
//     para.className='li-para';

//     let li=document.createElement('li');
//     li.className='task-item';
//     li.appendChild(para);
//     li.appendChild(button);
//     li.appendChild(span);

//     button.addEventListener('click',()=>{
//       editTask(li);
//     })

//     ulelement.appendChild(li);

//     updateTasks();
//     intialstatus();

//   })
// }

// // ---------------------- Showing All tasks --------
// allButton.addEventListener("click", (e) => {
//   ulelement
//     .querySelectorAll("li")
//     .forEach((task) => (task.style.display = "block"));
// });

// // - ------------------------- COMPLETED TASKS Showing ----------------
// completedButton.addEventListener("click", (e) => {
//   ulelement.querySelectorAll("li").forEach((task) => {
//     if (task.classList.contains("checked")) {
//       task.style.display = "block";
//     } else {
//       task.style.display = "none";
//     }
//   });
// });

// activeButton.addEventListener("click", (e) => {
//   ulelement.querySelectorAll("li").forEach((task) => {
//     if (task.classList.contains("checked")) {
//       task.style.display = "none";
//     } else {
//       task.style.display = "block";
//     }
//   });
// });

// clearCompletedButton.addEventListener('click',(e)=>{
//   ulelement.querySelectorAll('li.checked').forEach((task)=>{
//     task.remove();
//   })
//   updateTasks();
// })

// // --------------------------- Deleting AND updating the Tasks -------------
// ulelement.addEventListener("click", (e) => {
//   if (e.target.tagName === "LI") {
//     e.target.classList.toggle("checked");
//     updateTasks();
//   } else if (e.target.tagName === "I") {
//     // to find closest parent  element of li
//     let li = e.target.closest("li");
//     li.remove();
//     updateTasks();
//   }

//   if (ulelement.children.length === 0) {
//     window.location.reload();
//   }
// });

// addbutton.addEventListener("click", add);

// InitialiseTasks();

let addbutton = document.querySelector("#addbtn");
const status = document.querySelector(".status");
const inputBox = document.querySelector("#inputbox");
const taskList = document.querySelector(".elements");
const filters = document.querySelectorAll(".status button");
const allButton = status.querySelector("button:nth-of-type(1)");
const completedButton = status.querySelector("button:nth-of-type(2)");
const activeButton = status.querySelector("button:nth-of-type(3)");
const clearCompletedButton = status.querySelector("button:nth-of-type(4)");

let taskarray = [];

document.addEventListener("DOMContentLoaded", function () {
  // Your code here

  function add(tasktext) {
    taskarray.push({ text: tasktext, completed: false });
    rendertasks();
  }
  function rendertasks() {
    taskList.innerHTML = "";

    taskarray.forEach((task, index) => {
      const taskelement = createElement(task, index);
      taskList.appendChild(taskelement);
    });
    updateTasks();
  }

  function createElement(task, index) {
    const li = document.createElement("li");
    li.className = "task-item";
    li.innerHTML = `<p class='li-para'>${task.text}</p>
  <button class='edit-icon'>Edit</button>
  <span><i class='fas fa-trash trash-icon'></i></span>
  `;
    if (task.completed) {
      li.classList.add("checked");
    }

    const editbtn = li.querySelector(".edit-icon");
    editbtn.addEventListener("click", (e) =>
    {
      e.stopPropagation();
      editTask(index);
    })
    

    const deletebtn = li.querySelector(".trash-icon");
    deletebtn.addEventListener("click", (e) => {
      e.stopPropagation();
      deleteTask(index);
      updateTasks();
    });

    li.addEventListener("click", () => toggletask(index));

    return li;
  }
  function clearcompletetask() {
    taskarray = taskarray.filter((task) => !task.completed);
    rendertasks();
  }

  // function updateTasks() {
  //   const remainingtasks = taskList.querySelectorAll("li::unchecked").length;
  //   status.querySelector(
  //     "span p"
  //   ).textContent = `Tasks Remaining : ${remainingtasks}`;

  //   for (let index = 0; index < taskarray.length; index++) {
  //     console.log(taskarray[index]);

  //   }
  // }
  function editTask(index) {
    const li = taskList.children[index];
    const para = li.querySelector(".li-para");

    const inputfield = document.createElement("input");
    inputfield.type = "text";
    inputfield.value = para.textContent.trim();

    li.replaceChild(inputfield, para);
    inputfield.focus();

    function savetask() {
      const newText = inputfield.value.trim();
      if (newText !== "") {
        //For Updating the task in the array
          taskarray[index].text = newText; 
           // Updating the paragraph text to show the edited task 
          para.textContent = newText;
          //  Replace the input field back with the updated paragraph
          li.replaceChild(para, inputfield); 
          updateTasks();
      } else {
        
          console.log("Task text cannot be empty.");
          li.replaceChild(para, inputfield); // Optionally revert back if no new text is provided
      }
  }
    
    inputfield.addEventListener("blur", savetask);
    inputfield.addEventListener('keypress',(e)=>{
      if(e.key === "Enter"){
        e.preventDefault();
        savetask();
        inputfield.blur();
      }
    })
    
  }

  function deleteTask(index) {
    const tasktodelete = taskarray[index];
    if (tasktodelete !== undefined) {
      taskarray.splice(index, 1);
      updateTasks();
    }
    rendertasks();
  }

  function toggletask(index) {
    taskarray[index].completed = !taskarray[index].completed;
    rendertasks();
    console.log(taskarray[index]);
  }

  function updateTasks() {
    const remainingtask = taskarray.filter((task) => !task.completed).length;
    status.querySelector(
      "span p"
    ).textContent = `Tasks Remaining :${remainingtask}`;
    status.style.display = taskarray.length === 0 ? "none" : "flex";
  }

  function filtertask(filtertype) {
    taskList.querySelectorAll("li").forEach((task) => {
      const taskCompleted = task.classList.contains("checked");
      switch (filtertype) {
        case "all":
          task.style.display = "block";
          break;
        case "completed":
          task.style.display = taskCompleted ? "block" : "none";
          break;
        case "active":
          task.style.display = taskCompleted ? "none" : "block";
          break;
      }
    });
  }

  filters.forEach((filter) => {
    filter.addEventListener("click", (e) => {
      e.preventDefault();
      const filtertype = filter.dataset.filter;
      filtertask(filtertype);
    });
  });

  allButton.addEventListener("click", () => filtertask("all"));
  completedButton.addEventListener("click", () => filtertask("completed"));
  clearCompletedButton.addEventListener("click", () => clearcompletetask());
  activeButton.addEventListener("click", () => filtertask("active"));

  addbutton.addEventListener("click", (e) => {
    e.preventDefault();
    const tasktext = inputBox.value.trim();
    console.log("Clicked");
    if (tasktext !== "") {
      add(tasktext);
      inputBox.value = "";
      rendertasks();
    } else {
      alert("Write you task atleast");
    }
  });
  rendertasks();
});
